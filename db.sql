DROP TABLE IF EXISTS product;

CREATE TABLE product (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    category VARCHAR(255),
    label VARCHAR (255) NOT NULL,
    price DOUBLE NOT NULL,
    description TEXT
);

INSERT INTO product (category, label, price, description) VALUES 
('cycling', 'Touring bike', 120, 'A nice bike for touring'),
('cycling', 'Helmet', 20, 'Watch your head !'),
('animal', 'Dog Food', 10, 'Food, but for dogs');
