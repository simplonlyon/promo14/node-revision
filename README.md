## How To Use
1. Créer une bdd et importer dedans le fichier db.sql
2. Créer un fichier .env et mettre dedans la DATABASE_URL correspondant la bdd créée
3. `npm install` pour installer les dépendances
4. `npm start` pour lancer le projet
5. Données accessibles sur http://localhost:3000/api/product/category/cycling par exemple