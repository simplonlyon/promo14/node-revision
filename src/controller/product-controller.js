import { Router } from "express";
import { ProductRepository } from "../repository/ProductRepository";

export const productController = Router();


productController.get('/category/:category', async (req, res) => {
    try {
        const limit = isNaN(Number(req.query.limit)) ? 25:Number(req.query.limit);
        const offset = isNaN(Number(req.query.offset)) ? 0:Number(req.query.offset);
        const result = await ProductRepository.findByCategory(req.params.category, limit, offset);
        res.json(result);

    } catch(e) {
        console.log(e);
        res.status(500).json({
            message: 'Server Error'
        })
    }
});
