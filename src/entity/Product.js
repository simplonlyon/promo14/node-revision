export class Product {
    id;
    label;
    category;
    price;
    description;
    /**
     * 
     * @param {string} label 
     * @param {string} category 
     * @param {number} price 
     * @param {string} description 
     * @param {number} id 
     */
    constructor(label,category, price, description, id = null){
        this.id = id;
        this.label = label;
        this.category = category;
        this.price = price;
        this.description = description;

    }
}